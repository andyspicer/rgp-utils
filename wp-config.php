<?php
/*
This is the local-config.php file
In it, you *must* include the four main database defines

You may include other settings here that you only want enabled on your local development checkouts
*/
define('DB_NAME', 'wp_searchreplace');
define('DB_USER', 'wp_searchreplace');
define('DB_PASSWORD', 'wp_searchreplace');
define('DB_HOST', 'localhost');
define('DB_PREFIX', 'wp_');

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );


// =================================================================
// Debug mode
// Debugging? Enable these. Can also enable them in local-config.php
// =================================================================
define( 'SAVEQUERIES', true );
define( 'WP_DEBUG', true );